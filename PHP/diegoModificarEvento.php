<?php

$servername = "www.racni.com";
$username = "qrcodes";
$password = "F354c4tl4n";
$dbname = "1ncub4d0r4";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$idEvento = $_GET["idEvento"];
$nombreEvento = utf8_decode($_GET["nombreEvento"]);
$tipoDeEvento = utf8_decode($_GET["tipoDeEvento"]);
$fechaInicio = $_GET["fechaInicio"];
$fechaTermino = $_GET["fechaTermino"];
$tipoSede = utf8_decode($_GET["tipoSede"]);
$lugar = utf8_decode($_GET["lugar"]);
$tipoDeParticipacion =utf8_decode($_GET["tipoDeParticipacion"]);
$descripcion = utf8_decode($_GET["descripcion"]);
$cupo = $_GET["cupo"];
$numeroDeSesiones = $_GET["numeroDeSesiones"];
$nombrePonente = $_GET["nombrePonente"];
$registrosMinimos = $_GET["registrosMinimos"];
$registrosMaximos = $_GET["registrosMaximos"];
$habilitar = $_GET["habilitar"];
$hora = $_GET["hora"];


try {

    $stmt = $conn -> prepare("UPDATE Evento SET nombreEvento = ?,tipoDeEvento = ?, fechaInicio = ?, fechaTermino = ?,
       tipoSede = ?, lugar = ?, tipoDeParticipacion = ?, descripcion = ?, cupo = ?, numeroDeSesiones = ?,
       nombrePonente = ?, registrosMinimos = ?, registrosMaximos = ?, habilitar = ?, hora = ? WHERE idEvento = ?");

 $stmt -> bind_param("ssssssssiisiiisi", $nombreEvento, $tipoDeEvento, $fechaInicio, $fechaTermino, $tipoSede, $lugar,
 $tipoDeParticipacion, $descripcion, $cupo, $numeroDeSesiones,
    $nombrePonente, $registrosMinimos, $registrosMaximos, $habilitar, $hora, $idEvento);
 $stmt -> execute();

 $respuesta = array(
   "respuesta" => $conn -> error
 );



} catch (Exception $e) {

  $respuesta = array(
    "error" => $e->getMessage()
  );

}

echo json_encode($respuesta);

$result -> close();
$conn -> close();
$stmt -> close();


?>
