import { Injectable } from '@angular/core';
import { Lista } from '../models/lista.model';


@Injectable({
  providedIn: 'root'
})
export class MatsService {


  listas: Lista[] = [];

  constructor() {

    const lista1 = new Lista('Materiales Entrega');
    const lista2 = new Lista('Materiales Recibe');

    this.listas.push(lista1, lista2);
    console.log(this.listas);
   }


}
