import { TestBed } from '@angular/core/testing';

import { MatsService } from './mats.service';

describe('MatsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MatsService = TestBed.get(MatsService);
    expect(service).toBeTruthy();
  });
});
