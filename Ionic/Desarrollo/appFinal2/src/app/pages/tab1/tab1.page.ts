import { Component } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { DataLocalService } from '../../services/data-local.service';
import { RouterLink, Router } from '@angular/router';
import { NavController } from '@ionic/angular';

/* Conexión  con BD */
import { HttpClient } from "@angular/common/http";




@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  id: string;
  rfc: string;
  listado;

  constructor(private barcodeScanner: BarcodeScanner,
              private dataLocal: DataLocalService,
              private router: Router,
              private navCtrl: NavController,
              private http: HttpClient ) {
                /* Conexion PROFESOR con BD */
                this.http.get('http://localhost/micrudtutoapp/src/api/productos.php?opcion=1').subscribe(snap => {
                console.log(snap);
                this.listado = snap;
    });
              }


////////////////////////////////////////////////////////////////////////
              /* ESCANEO DE PROFESORES Y GUARDADO DE DATOS*/
  scan() {
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
      if ( !barcodeData.cancelled ) {
        this.dataLocal.guardarRegistro( barcodeData.text );
        this.router.navigate(['/materiales']);
      }

     }).catch(err => {
         console.log('Error', err);
     });
  }



  registrarID( id: string ) {
    console.log('Entre a ID');

    if ( !id ) {
      console.log('Error');
      throw new Error('No ha ingresado nada alv');
    }

    this.router.navigate(['/materiales']);
    id = '';
    /*
    Recibir un Input y enviar a la base
    this.dataLocal.guardarRegistro( barcodeData.text );
    */

  }

  registrarRFC( rfc: string ) {
    console.log('Entre a RFC');

    if ( !rfc ) {
      console.log('Error');
      throw new Error('No ha ingresado nada alv');
    }

    this.router.navigate(['/materiales']);
    rfc = '';
    /*
    Recibir un Input y enviar a la base
    this.dataLocal.guardarRegistro( barcodeData.text );
    */
  }




}
