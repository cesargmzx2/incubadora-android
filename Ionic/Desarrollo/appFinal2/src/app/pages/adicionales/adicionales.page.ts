import { Component, OnInit, ViewChild } from '@angular/core';
import { MatsService } from 'src/app/services/mats.service';
import { IonSegment } from '@ionic/angular';

@Component({
  selector: 'app-adicionales',
  templateUrl: './adicionales.page.html',
  styleUrls: ['./adicionales.page.scss'],
})
export class AdicionalesPage implements OnInit {

  @ViewChild(IonSegment, { static: true }) segment: IonSegment;

  constructor(public matsService: MatsService) { }


  ngOnInit() {
  }

}
