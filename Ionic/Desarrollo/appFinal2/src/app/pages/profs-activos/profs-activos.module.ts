import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfsActivosPageRoutingModule } from './profs-activos-routing.module';

import { ProfsActivosPage } from './profs-activos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfsActivosPageRoutingModule
  ],
  declarations: [ProfsActivosPage]
})
export class ProfsActivosPageModule {}
