import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProfsActivosPage } from './profs-activos.page';

describe('ProfsActivosPage', () => {
  let component: ProfsActivosPage;
  let fixture: ComponentFixture<ProfsActivosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfsActivosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProfsActivosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
