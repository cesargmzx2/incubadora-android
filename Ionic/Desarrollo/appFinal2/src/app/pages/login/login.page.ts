import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';


// import { IonLoading, IonButton, IonContent } from '@ionic/react';
/* imports para BD */
import { HttpClient } from '@angular/common/http';
// import { Http, Headers, RequestOptions } from '@angular/http';

// import 'rxjs/add/operator/map';
import { LoadingController, AlertController } from '@ionic/angular';
import { Registro } from '../../models/registro.model';
import { Logeo } from '../../models/logeo.model';


@Component({

  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  // Variables

  usuario: string;
  password: string;
  res;
  loading: any;
  logeo = new Logeo('', '' );
  router: any;

  // Fin variables



  //// CONTRUCTOR

  constructor(private alertCtrl: AlertController,
              private http: HttpClient,
              public loadingCtrl: LoadingController
    ) { }



  ngOnInit() {
  }

  onSubmitTemplate() {
    console.log('Form submit');
  }

  /* Activa formulario solo si se llenan los campos */
  login(formulario: NgForm) {
    console.log(formulario.valid);
  }



 /* Easter sin relación */
    async presentAlert() {
    const alert =  await this.alertCtrl.create({
      header: 'Desarrollo Incubadora Acatlán',
      subHeader: 'Contacto',
      message: 'ss.ti.incubadora@gmail.com',
      buttons: ['OK']
    });

    alert.present();
  }



    /* Logeo con Base de Datos  */

    async signIn() {

           this.logeo  = {
           username: this.usuario,
           password: this.password
          };
           // console.log (this.logeo);

   /* Crear loader en lo que se valida la información */
           this.presentLoading();
           setTimeout(() => {
          }, 150);
          /* Enviar info  */
           this.res = this.enviar( this.logeo );

           /* Se cierra el loading se muestra respuesta */

           if (this.res === 0) {

            console.log('Datos correctos');

            this.router.navigate(['/materiales']);
         } else {

          console.log ('Datos invalidos');
         }



        }


 enviar( logeo: Logeo ) {
  let x = 0;
  x += 1;

  return x;
 }



 async presentLoading(  ) {
   this.loading = await this.loadingCtrl.create({
    message: 'Espere un momento ',
     // duration: 2000
  });
   return this.loading.present();
}











        /* loader = await this.loading.create({
           content: 'Cargando',
         });
         loader.present().then(() => { */

    /*this.http.post ('http:http:www.zafiraconsulting.com.mx/this.login.php', data, options)
    .map(res => res.json())
    .subscribe(async res => {
    console.log(res);
     Cerrar loader
    loader.dismiss(); */

          /* Validar respuesta */
/*
    if (res === 'Your; Login; success;') {
    const alert = await this.alertCtrl.create({
      header: 'Bienvenido',
      subHeader: (res),
      buttons: ['OK']
    });
    alert.present();
    } else {
      const alert = await this.alertCtrl.create({
        header: 'ERROR',
        subHeader: 'Su usuario o contraseña son invalidos',
        buttons: ['OK']
      });
      alert.present();
  }});
});
*/

}
