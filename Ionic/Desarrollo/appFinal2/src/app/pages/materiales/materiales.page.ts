import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSegment } from '@ionic/angular';
import { MatsService } from '../../services/mats.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { DataLocalService } from '../../services/data-local.service';
import { RouterLink, Router } from '@angular/router';
import { NavController } from '@ionic/angular';
import { ListaItem } from '../../models/lista-item.model';
import { Lista } from '../../models/lista.model';

@Component({
  selector: 'app-materiales',
  templateUrl: './materiales.page.html',
  styleUrls: ['./materiales.page.scss'],
})
export class MaterialesPage implements OnInit {
  data: any[] =  Array(5);
  lista = new Lista('Materiales');
  nombreItem = '';

  @ViewChild(IonSegment, { static: true }) segment: IonSegment;

  constructor(public matsService: MatsService,
              private barcodeScanner: BarcodeScanner,
              private dataLocal: DataLocalService,
              private router: Router,
              private navCtrl: NavController) { }


////////////////////////////////////////////////////////////////////////

agregarItem() {
  if (this.nombreItem.length === 0) {
    return;
  }

  const nuevoItem = new ListaItem( this.nombreItem);
  this.lista.items.push( nuevoItem);
}

  scan() {
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
      if ( !barcodeData.cancelled ) {
        this.dataLocal.guardarRegistro( barcodeData.text );
        this.nombreItem = barcodeData.text;
        // this.router.navigate(['/materiales']);
      }

     }).catch(err => {
         console.log('Error', err);
     });
  }


  delete( i: number ) {
    this.lista.items.splice( i, 1 );
  }








  ngOnInit() {
    this.segment.value = 'llaves';
  }

  segmentChanged( event) {
    const valorSegmento = event.detail.value;
    console.log(valorSegmento);
  }

}
