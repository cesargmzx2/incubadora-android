import { Component, OnInit } from '@angular/core';
import { MatsService } from 'src/app/services/mats.service';

@Component({
  selector: 'app-resultado',
  templateUrl: './resultado.page.html',
  styleUrls: ['./resultado.page.scss'],
})
export class ResultadoPage implements OnInit {

  constructor(public matsService: MatsService) { }


  ngOnInit() {
  }

}
