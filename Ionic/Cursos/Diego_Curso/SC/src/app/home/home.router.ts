import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';
import { RfcPage } from '../pages/rfc/rfc.page';
import { RfcPageModule } from '../pages/rfc/rfc.module';

const routes: Routes = [
  {
    path: 'home',
    component: HomePage,
    children: [
        {
            path: 'escaner',
            loadChildren: () => import('../pages/escaner/escaner.module').then(
                m => m.EscanerPageModule
            )
        },
        {
            path: 'id',
            loadChildren: () => import('../pages/id/id.module').then(
                m => m.IdPageModule
            )
        },
        {
            path: 'rfc',
            loadChildren: () => import('../pages/rfc/rfc.module').then(
                m => m.RfcPageModule
            )
        }
     ]
}];


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class HomeRouter {}
