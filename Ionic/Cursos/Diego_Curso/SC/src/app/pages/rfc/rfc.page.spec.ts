import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RfcPage } from './rfc.page';

describe('RfcPage', () => {
  let component: RfcPage;
  let fixture: ComponentFixture<RfcPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RfcPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RfcPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
