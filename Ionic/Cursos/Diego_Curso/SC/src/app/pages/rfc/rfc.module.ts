import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RfcPageRoutingModule } from './rfc-routing.module';

import { RfcPage } from './rfc.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RfcPageRoutingModule
  ],
  declarations: [RfcPage]
})
export class RfcPageModule {}
