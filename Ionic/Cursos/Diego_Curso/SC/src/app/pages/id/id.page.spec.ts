import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IdPage } from './id.page';

describe('IdPage', () => {
  let component: IdPage;
  let fixture: ComponentFixture<IdPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IdPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IdPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
