package com.example.evento;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;




public class QRActivity extends AppCompatActivity {
    String IP = "https://www.racni.com/2019/EventosIncubadora/";
    final Activity activity = this;


    Button botonScan,botonRegistroId,botonRegistroMail;
    Spinner spinner;
    String scannedData="";

    ArrayList<String> nombre_Eventos = new ArrayList<String>();
    ArrayList<Integer> id_Eventos = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr);

        botonScan = findViewById(R.id.btnScan);

        botonRegistroId =findViewById(R.id.btnID);

        botonRegistroMail = findViewById(R.id.btnCorreo);

        spinner = findViewById(R.id.spinnerEventos);
        EditText idText;

        Connection connection = new Connection();

        try {

            String response = connection.execute(IP+"diegoObtenerEvento.php").get();

            JSONArray jsonArray = new JSONArray(response);

            for (int i = 0; i< jsonArray.length(); i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                nombre_Eventos.add(jsonObject.getString("nombreEvento"));
                id_Eventos.add(jsonObject.getInt("idEvento"));

            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayAdapter<String> a = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, nombre_Eventos);
        spinner.setAdapter(a);

        botonScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator Integrator = new IntentIntegrator(activity);
                Integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
                Integrator.setPrompt("Registro QR");
                Integrator.setBeepEnabled(true);
                Integrator.setCameraId(0);
                Integrator.setBarcodeImageEnabled(false);
                Integrator.initiateScan();

            }
        });}


        @Override
        protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
            super.onActivityResult(requestCode, resultCode, data);


            IntentResult result = IntentIntegrator.parseActivityResult(requestCode,resultCode,data);

            if(result!=null) {

                scannedData = result.getContents();
                if (scannedData != null) {

                    Log.d("HOLA",scannedData);

                    Connection connection = new Connection();

                    try {

                        String response =  connection.execute(IP+"asistenciaUsuario.php?hash=" + scannedData + "&evento=1").get();

                        JSONArray jsonArray = new JSONArray(response);

                        if (jsonArray.length() > 0){

                            JSONObject jsonObject = jsonArray.getJSONObject(0);

                            String resp;
                            resp = jsonObject.getString("respuesta");

                            if (resp != null){
                                Toast.makeText(QRActivity.this,"Respuesta "+resp, Toast.LENGTH_SHORT).show();
                                if (resp == ""){
                                    Toast.makeText(QRActivity.this,"registro exitoso", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                String error = jsonObject.getString("error");
                                Toast.makeText(QRActivity.this,"Error "+error, Toast.LENGTH_SHORT).show();
                            }


                        }else {
                            Toast.makeText(QRActivity.this,"Error extraño", Toast.LENGTH_SHORT).show();
                        }

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(QRActivity.this,""+e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }

    }

    /*
    *
    * 25/09/2019
    * Diego Antonio Colín Castañeda
    *
    *
    *
    *
    * Error Scanner
    * " Value ERROR of type java.lang.String cannot be converted to JSONArray "
    *
    *
    *
    *
    *
    * */