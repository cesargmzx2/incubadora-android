package com.example.evento;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;



public class ModificarActivity extends AppCompatActivity {

    String IP = "https://www.racni.com/2019/EventosIncubadora/";

    /*Texto de autocompletado*/

    private static final String[] LUGARESS = new String[]{
            "Sala de videoconferencias UIM II","Unidad de congresos UIM II","Aula Magna UIM II","Departamento de proyección Empresarial UIM II"

    };

    private static final String[] Tipo = new String[]{
            "Taller","Conferencia"

    };


    Calendar c,c2;
    DatePickerDialog dpd,dpd2;
    Integer Completo =1;
    Spinner spinner;
    Connection connection = new Connection();
    Connection connection1, Connection2, connection3, connection4, connection5;
    ArrayList<String> nombre_Evento = new ArrayList<String>();
    ArrayList<Integer> id_Evento = new ArrayList<Integer>();
    TextView FechaI,FechaT;
    String sNombreE, sTipoE, sFechaI, sFechaT, sTipoSede, sLugar, sParticipacion, sDescripcion, sCupo, sSesiones, sOrganizador, sRegMin, sRegMax, sHora, shabilitado, Seleccion, Seleccion2;
    Integer indiceEvento;
    String Seleccion3;
    EditText NombreE, Organizador, Descripcion, Cupo, Sesiones, RegMin, RegMax, Hora;
    RadioGroup Contenedor,Contenedor2;
    RadioButton Nac, Inter, org, col;
    CheckBox hab;
    Button BTnFechaI, BTnFechaT;
    AutoCompleteTextView Lugar,TipoE;
    private Button modifica;
   Button btnElimina;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificar);

        TipoE   = findViewById(R.id.tpoEvento);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,Tipo);
        TipoE.setAdapter(adapter);


        Lugar = findViewById(R.id.textLugar);
        ArrayAdapter<String> adapter4 = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,LUGARESS);
        Lugar.setAdapter(adapter4);


        FechaI = findViewById(R.id.fecha1);
        BTnFechaI =findViewById(R.id.btnfecha1);


        BTnFechaI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c = Calendar.getInstance();
                int day =c.get(Calendar.DAY_OF_MONTH);
                int month = c.get(Calendar.MONTH);
                int year = c.get(Calendar.YEAR);

                dpd = new DatePickerDialog(ModificarActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int mYear, int mMonth, int mDay) {
                        FechaI.setText(mYear+"-"+(mMonth+1)+"-"+mDay);
                    }
                },year, month,day );
                dpd.show();
            }

        });


        BTnFechaT =findViewById(R.id.btnfecha2);
        FechaT = findViewById(R.id.fecha2);


        BTnFechaT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c2 = Calendar.getInstance();
                int day =c2.get(Calendar.DAY_OF_MONTH);
                int month = c2.get(Calendar.MONTH);
                int year = c2.get(Calendar.YEAR);

                dpd2 = new DatePickerDialog(ModificarActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int mYear, int mMonth, int mDay) {
                        FechaT.setText(mYear+"-"+(mMonth+1)+"-"+mDay);
                    }
                },year, month, day);
                dpd2.show();
            }

        });


        // Conectar java con xml
        spinner = (Spinner) findViewById(R.id.spinnerM);
        NombreE = findViewById(R.id.nbrEvento);
        //TipoE = findViewById(R.id.tpoEvento);
        //FechaI = findViewById(R.id.fecha1);
        //FechaT = findViewById(R.id.fecha2);
        //Lugar = findViewById(R.id.Lugar);
        Organizador = findViewById(R.id.texOrg);
        Descripcion = findViewById(R.id.Descripcion);
        Cupo = findViewById(R.id.Cupo);
        Sesiones = findViewById(R.id.sesiones);
        RegMin = findViewById(R.id.regMin);
        RegMax = findViewById(R.id.regMax);
        Hora = findViewById(R.id.hora);
        Contenedor = (RadioGroup) findViewById(R.id.tipoSede);
        Contenedor2 = (RadioGroup) findViewById(R.id.tipoDeParticipacion);
        Nac = findViewById(R.id.rbNac);
        Inter = findViewById(R.id.rbInt);
        org = findViewById(R.id.rbparticipacion1);
        col = findViewById(R.id.rbparticipacion2);
        hab = findViewById(R.id.habilitado);
        modifica = findViewById(R.id.btnConfirma);

        btnElimina = findViewById(R.id.BtnElim);

        //LLenado de nuestros datos
        connection = new Connection();

        try {

            String response = connection.execute(IP+"diegoObtenerEvento.php").get();

            JSONArray jsonArray = new JSONArray(response);

            for (int i = 0; i< jsonArray.length(); i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                nombre_Evento.add(jsonObject.getString("nombreEvento"));
                id_Evento.add(jsonObject.getInt("idEvento"));

            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayAdapter<String> a = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, nombre_Evento);
        spinner.setAdapter(a);



        /*Ver seleccion de usuario en spinner*/

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                indiceEvento = position;
                connection1 = new Connection();



                try {
                    String response = connection1.execute(IP + "diegoObtenerEventoCompleto.php?evento=" + Integer.toString(id_Evento.get(indiceEvento))).get();

                    JSONArray jsonArray = new JSONArray(response);


                    if (jsonArray.length() > 0) {

                        JSONObject jsonObject = jsonArray.getJSONObject(0);

                        NombreE.setText(jsonObject.getString("nombreEvento"));
                        TipoE.setText(jsonObject.getString("tipoDeEvento"));
                        FechaI.setText(jsonObject.getString("fechaInicio"));
                        FechaT.setText(jsonObject.getString("fechaTermino"));
                        Lugar.setText(jsonObject.getString("lugar"));
                        Descripcion.setText(jsonObject.getString("descripcion"));
                        Cupo.setText(jsonObject.getString("cupo"));
                        Sesiones.setText(jsonObject.getString("numeroDeSesiones"));
                        Organizador.setText(jsonObject.getString("nombrePonente"));
                        RegMin.setText(jsonObject.getString("registrosMinimos"));
                        RegMax.setText(jsonObject.getString("registrosMaximos"));
                        Hora.setText(jsonObject.getString("hora"));


                        //TipoSede

                        Seleccion = jsonObject.getString("tipoSede");
                        if (Seleccion.equals("Nacional")) {
                            Nac = (RadioButton) Contenedor.getChildAt(0);
                            Nac.setChecked(true);
                            //Contenedor.check(Nac.getId());
                        } else {
                            if (Seleccion.equals("Internacional")) {
                                Inter = (RadioButton) Contenedor.getChildAt(1);
                                Inter.setChecked(true);
                            } else {
                                Toast.makeText(ModificarActivity.this, "Error al obtener ", Toast.LENGTH_SHORT).show();
                            }
                        }


                        //tipoDeParticipacion
                        Seleccion2 = jsonObject.getString("tipoDeParticipacion");
                        if (Seleccion2.equals("Organizador")) {
                            org = (RadioButton) Contenedor2.getChildAt(0);
                            org.setChecked(true);
                        } else {
                            if (Seleccion2.equals("Colaborador")) {
                                col = (RadioButton) Contenedor2.getChildAt(1);
                                col.setChecked(true);
                            } else {
                                Toast.makeText(ModificarActivity.this, "Error al obtener1", Toast.LENGTH_SHORT).show();
                            }
                        }


                        //habilitar
                        Seleccion3 = jsonObject.getString("habilitar");
                        if (Seleccion3.equals("1")) {
                            hab.setChecked(true);
                        }
                    } else {
                        Toast.makeText(ModificarActivity.this, "Error extraño", Toast.LENGTH_SHORT).show();
                    }

                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }


        });





        /* Boton Modificar*/


        modifica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /*Recopilación de datos del View*/

                sNombreE = NombreE.getText().toString();
                sTipoE = TipoE.getText().toString();
                sFechaI = FechaI.getText().toString();
                sFechaT = FechaT.getText().toString();
                if (Nac.isChecked()) {

                    sTipoSede = "Nacional";


                } else{ if (Inter.isChecked()) {

                    sTipoSede = "Internacional";

                }}

                sLugar = Lugar.getText().toString();

                if (org.isChecked()) {

                    sParticipacion = "Organizador";


                } else{ if (col.isChecked()) {

                    sParticipacion = "Colaborador";

                }}

                sDescripcion = Descripcion.getText().toString();
                //Aqui
                sCupo = Cupo.getText().toString();

                sSesiones = Sesiones.getText().toString();

                sOrganizador = Organizador.getText().toString();
                //Aqui
                sRegMin = RegMin.getText().toString();

                sRegMax = RegMax.getText().toString();

                sHora = Hora.getText().toString();
                //Aqui
                if (hab.isChecked()==true) {shabilitado = "1";}
                else{ shabilitado = "0";}


                connection4 = new Connection();

                try {
                    connection4.execute(IP + "diegoModificarEvento.php?idEvento=" + id_Evento.get(indiceEvento).toString() + "&nombreEvento="
                            + sNombreE + "&tipoDeEvento=" + sTipoE + "&fechaInicio=" + sFechaI + "&fechaTermino=" + sFechaT + "&tipoSede=" + sTipoSede
                            + "&lugar=" + sLugar + "&tipoDeParticipacion=" + sParticipacion
                            + "&descripcion=" + sDescripcion + "&cupo=" + sCupo + "&numeroDeSesiones=" + sSesiones
                            + "&nombrePonente=" + sOrganizador + "&registrosMinimos=" + sRegMin + "&registrosMaximos=" + sRegMax + "&hora="+ sHora
                            + "&habilitar=" + shabilitado).get();
                            Toast.makeText(ModificarActivity.this, "Evento Modificado con éxito", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ModificarActivity.this, MenuActivity.class);
                    startActivity(intent);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }


            }
        });


        btnElimina .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alerta = new AlertDialog.Builder(ModificarActivity.this);
                alerta.setMessage("Esta accion no se puede deshacer ¿Desea continuar?")
                        .setCancelable(false)
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    connection5 = new Connection();
                                    connection5.execute(IP + "diegoBorrarEvento.php?idEvento=" + id_Evento.get(indiceEvento).toString()).get();
                                    Toast.makeText(ModificarActivity.this, "Evento Eliminado con éxito", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(ModificarActivity.this, MenuActivity.class);
                                    startActivity(intent);

                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                } catch (ExecutionException e) {
                                    e.printStackTrace();
                                }
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                AlertDialog titulo =alerta.create();
                titulo.setTitle("AVISO");
                titulo.show();

            }
        });

    }



/*
        @Override
            public void onNothingSelected(AdapterView<?> parent) {
                indiceEvento = 0;
            }
        });}


    }*/
}



/*
 *
 * 25/09/2019
 * Diego Antonio Colín Castañeda
 *
 *
 * ERROR CONEXIÓN CON BD
 * LISTO
 *
 *
 *FALTA:
 *
 * Jalar datos del evento de la BD para llenar formulario listo
 * Enviar a BD cambios
 *
 *
 *
 *
 *
 *
 * */