package com.example.evento;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.ExecutionException;

import javax.microedition.khronos.egl.EGLDisplay;

public class PruebaActivity extends AppCompatActivity {


    /*Texto de autocompletado*/

    private static final String[] LUGARES = new String[]{
            "Sala de videoconferencias UIM II","Unidad de congresos UIM II","Aula Magna UIM II","Departamento de proyección Empresarial UIM II"

    };

    private static final String[] Tipo = new String[]{
            "Taller","Conferencia"

    };

    /* Datos */
    Calendar c,c2;
    DatePickerDialog dpd,dpd2;
    TextView FechaI,FechaT;
    EditText NombreE, Organizador, Descripcion, Cupo, Sesiones, RegMin , RegMax, Hora;
    RadioButton Nac, Inter, org, col;
    CheckBox hab;
    Button confirm;
    String cero = "0";
    String sNombreE, sTipoE, sFechaI, sFechaT, sTipoSede, sLugar,sParticipacion, sDescripcion, sCupo,sSesiones, sOrganizador, sRegMin , sRegMax, sHora, shabilitado;
    Connection connection3;
    String IP =  "https://www.racni.com/2019/EventosIncubadora/";
    Button BTnFechaI, BTnFechaT;
    String resp;
    AutoCompleteTextView Lugar, TipoE;



    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prueba);


        TipoE   = findViewById(R.id.tpoEvento);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,Tipo);
        TipoE.setAdapter(adapter);

        Lugar = findViewById(R.id.texLugar);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,LUGARES);
        Lugar.setAdapter(adapter1);


        BTnFechaI =findViewById(R.id.btnfecha1);
        FechaI = findViewById(R.id.fecha1);


        BTnFechaI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c = Calendar.getInstance();
                int day =c.get(Calendar.DAY_OF_MONTH);
                int month = c.get(Calendar.MONTH);
                int year = c.get(Calendar.YEAR);

                dpd = new DatePickerDialog(PruebaActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int mYear, int mMonth, int mDay) {
                        FechaI.setText(mYear+"-"+(mMonth+1)+"-"+mDay);
                    }
                },year, month,day);
                dpd.show();
            }

        });


        BTnFechaT =findViewById(R.id.btnfecha2);
        FechaT = findViewById(R.id.fecha2);


        BTnFechaT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c2 = Calendar.getInstance();
                int day =c2.get(Calendar.DAY_OF_MONTH);
                int month = c2.get(Calendar.MONTH);
                int year = c2.get(Calendar.YEAR);

                dpd2 = new DatePickerDialog(PruebaActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int mYear, int mMonth, int mDay) {
                        FechaT.setText(mYear+"-"+(mMonth+1)+"-"+mDay);
                    }
                },year, month,day);
                dpd2.show();
            }

        });



        NombreE = findViewById(R.id.nbrEvento);
        //TipoE   = findViewById(R.id.tpoEvento);
        //FechaI = findViewById(R.id.fecha1);
        //FechaT = findViewById(R.id.fecha2);
        //Lugar = findViewById(R.id.texLugar);
        Organizador = findViewById(R.id.texOrg);
        Descripcion  = findViewById(R.id.descripcion);
        Cupo  = findViewById(R.id.cupo);
        Sesiones = findViewById(R.id.sesiones);
        RegMin = findViewById(R.id.regMin);
        RegMax = findViewById(R.id.regMax);
        Hora   = findViewById(R.id.hora);
        Nac =findViewById(R.id.rbNac);
        Inter  = findViewById(R.id.rbInt);
        org = findViewById(R.id.rbparticipacion1);
        col = findViewById(R.id.rbparticipacion2);
        hab  = findViewById(R.id.habilitado);
        confirm = findViewById(R.id.btnConfirma);

        Nac.setChecked(true);
        org.setChecked(true);


     }


    /* Boton enviar*/

    public void Enviar (View v) {


        /*Recopilación de datos del View*/

        sNombreE = NombreE.getText().toString();
        sTipoE = TipoE.getText().toString();
        sFechaI = FechaI.getText().toString();
        sFechaT = FechaT.getText().toString();
        if (Nac.isChecked()) {

            sTipoSede = "Nacional";


        } else{ if (Inter.isChecked()) {

            sTipoSede = "Internacional";

        }}

        sLugar = Lugar.getText().toString();

        if (org.isChecked()) {

            sParticipacion = "Organizador";


        } else{ if (col.isChecked()) {

            sParticipacion = "Colaborador";

        }}

        sDescripcion = Descripcion.getText().toString();

        sCupo = Cupo.getText().toString();

        sSesiones = Sesiones.getText().toString();

        sOrganizador = Organizador.getText().toString();

        sRegMin = RegMin.getText().toString();

        sRegMax = RegMax.getText().toString();

        sHora = Hora.getText().toString();

        if (hab.isChecked()==true) {shabilitado = "1";}
        else{ shabilitado = "0";}

            if(sNombreE.isEmpty()|| sTipoE.isEmpty()|| sFechaI.isEmpty()|| sFechaT.isEmpty()|| sLugar.isEmpty()|| sDescripcion.isEmpty()|| sCupo.isEmpty()|| sSesiones.isEmpty()|| sOrganizador.isEmpty()|| sRegMin.isEmpty()|| sRegMax.isEmpty()|| sHora.isEmpty()){

                Toast.makeText(PruebaActivity.this,"Llena todos los datos",Toast.LENGTH_SHORT).show();
                //Toast.makeText(PruebaActivity.this,IP +"nombreEvento="+sNombreE+"&tipoEvento=" + sTipoE + "&fechaInicio=" + sFechaI + "&fechaTermino=" + sFechaT + "&tipoSede=" + sTipoSede + "&lugar=" + sLugar + "&tipoDeParticipacion=" + sParticipacion + "&descripcion=" + sDescripcion + "&cupo=" + sCupo + "&numeroDeSesiones=" + sSesiones + "&nombrePonente=" + sOrganizador + "&registrosMinimos=" + sRegMin + "&registrosMaximos=" + sRegMax+ "&habilitar=" + shabilitado + "&hora="+ sHora,Toast.LENGTH_LONG).show();

             }else{
                connection3 = new Connection();
                try {
                    connection3.execute(IP + "diegoEnviarEvento.php?nombreEvento="+sNombreE+"&tipoDeEvento=" + sTipoE
                            + "&fechaInicio=" + sFechaI + "&fechaTermino=" + sFechaT + "&tipoSede=" + sTipoSede + "&lugar=" + sLugar + "&tipoDeParticipacion="
                            + sParticipacion + "&descripcion=" + sDescripcion + "&cupo=" + sCupo + "&numeroDeSesiones=" + sSesiones + "&nombrePonente=" + sOrganizador
                            + "&registrosMinimos=" + sRegMin + "&registrosMaximos=" + sRegMax+ "&habilitar=" + shabilitado + "&hora="+ sHora).get();

                    Toast.makeText(PruebaActivity.this, "Evento creado con éxito", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(PruebaActivity.this, MenuActivity.class);
                    startActivity(intent);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }}
}





        /*
 *
 * 09/10/2019
 * Diego Antonio Colín Castañeda
 *
 *
 * BUGS:
 *
 * ERROR CONEXIÓN CON BD
 *
 *
 *
 *FALTA:
 *
 *
 * Validar todos los campos llenos para enviar      LISTO
 * DatePicker                                       LISTO
 * Predefinido Nacional y Organizador               LISTO
 * Lugar (4 Lugares UIM)                            LISTO
 *
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */